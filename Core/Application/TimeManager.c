
#include "TimeManager.h"

extern ActionsFlags Flags;

int debug_2=0;

//______________________________________________
void TimeManagerInit(TIM_HandleTypeDef * ChosenTimer);
void TimeControl(DelayTicks* Ticks);
static void IncreaseTicks(DelayTicks* Ticks);
void IWDG_Refresh(IWDG_HandleTypeDef *Watchdog);
//______________________________________________

void TimeManagerInit(TIM_HandleTypeDef * ChosenTimer)
{
	HAL_TIM_Base_Start_IT(ChosenTimer);
}

static void IncreaseTicks(DelayTicks* Ticks)
{
	/* Increase each tick */
	Ticks->InputsTick++;
	Ticks->ThermistorTick++;
	Ticks->ThrottleTick++;
	Ticks->TemperatureSensorTick++;
	Ticks->TwillightTick++;
}

void TimeControl(DelayTicks* Ticks)
{
	/* Functions increasing and control current ticks for each periodical action */
	debug_2++;

	if(Ticks->ThermistorTick >= THERMISTOR_DELAY)
	{
		Flags.ThermistorFlag = true;
		Ticks->ThermistorTick = 0;
	}


	if(Ticks->InputsTick >= INPUT_DELAY)
	{
		// TAKE SOME ACTIONS....
		Flags.InputFlag = true;
		Ticks->InputsTick = 0; // reset tick for an action

	}


	if(Ticks->ThrottleTick >= THROTTLE_DELAY)
	{
		// TAKE SOME ACTIONS....
		Flags.ThrottleFlag = true;
		Ticks->ThrottleTick = 0; // reset tick for an action

	}
	if(Ticks->TemperatureSensorTick >= TMPSENSOR_DELAY)
		{
			// TAKE SOME ACTIONS....
			Flags.TemperatureSensorFlag = true;
			Ticks->TemperatureSensorTick = 0; // reset tick for an action

		}
	if(Ticks->TwillightTick >= TWILLIGHT_DELAY)
	{
		Flags.TwillightFlag=true;
		Ticks->TwillightTick=0;
	}

	/* Increase each tick */
	IncreaseTicks(Ticks);

}

void IWDG_Refresh(IWDG_HandleTypeDef *Watchdog)
{
	HAL_IWDG_Refresh(Watchdog); // reload watchdog timer register
}


