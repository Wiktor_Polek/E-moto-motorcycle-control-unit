/*
 * CanApp.h
 *
 *  Created on: Nov 29, 2020
 *      Author: Dawid
 */

#ifndef APPLICATION_CANAPP_H_
#define APPLICATION_CANAPP_H_

#include "stm32l4xx_hal.h"
#include "can.h"
#include "stdbool.h"

#define 	CAN_FRAME_DATA_SIZE		8

void CanInit(CAN_HandleTypeDef *hcan);
bool CanSendFrame(uint32_t ID, uint8_t* data, uint8_t size);
bool CanRqstData(uint32_t ID);


typedef struct
{
	uint8_t ID;
	uint8_t TxData[CAN_FRAME_DATA_SIZE];
	uint8_t RxData[CAN_FRAME_DATA_SIZE];
}CanMsg;
#endif /* APPLICATION_CANAPP_H_ */
