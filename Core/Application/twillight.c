/*
 * twillight.c
 *
 *  Created on: Apr 19, 2021
 *      Author: Olson
 */
#include"twillight.h"


TLight_Data_t ADCTest;
int calculateAvarageValue(uint16_t v);

void twillightInit(ADC_HandleTypeDef *hadc,TLight_Data_t *t,bool isDefaultValueOn,uint8_t limitThresholdOn)
{
	t->previousIsLightOn=0;
	t->isLightOn=0;
	if(isDefaultValueOn==1)
	{
		t->limitThresholdOn=DEFAULT_VALUE_CHANGE;
		t->limitThresholdOff=t->limitThresholdOn-DIFFERENCE_HYSTERESIS;
	}
	else
	{
		t->limitThresholdOn=limitThresholdOn;
		t->limitThresholdOff=t->limitThresholdOn-DIFFERENCE_HYSTERESIS;
	}

}

 ADC_Status twillightLoop(TLight_Data_t *t,ADC_HandleTypeDef *hadc)
{
	ADC_SetActiveChannel(hadc, ADC_CHANNEL_9);
	HAL_ADC_Start(hadc);
	if (HAL_ADC_PollForConversion(hadc, 3) == HAL_OK) {
		t->adcValue = HAL_ADC_GetValue(hadc);
		if (calculateAvarageValue(t->adcValue) == -1) {
			t->avarageValue = t->adcValue;
		} else {
			t->avarageValue = calculateAvarageValue(t->adcValue);
		}
		t->twillightLevel = (uint8_t) (t->avarageValue * VALUE_MAX_ADC_8BIT
				/ VALUE_MAX_ADC_12BIT);

		if (t->limitThresholdOn <= t->twillightLevel) {
			t->isLightOn = 1;
			t->previousIsLightOn = 1;
		} else if (t->limitThresholdOff >= t->twillightLevel) {
			t->isLightOn = 0;
			t->previousIsLightOn = 0;
		} else {
			t->isLightOn = t->previousIsLightOn;
		}
		return ADC_OK;
	} else {
		return ADC_ERROR;
	}

}
int calculateAvarageValue(uint16_t v)
{
	unsigned int avarageValue=0;
	static uint8_t isFirstValues=1;
	static uint8_t licz=0;
	static uint16_t tab[NUMBER_OF_SAMPLES];
	if(licz<NUMBER_OF_SAMPLES)
	{
		licz++;
	}
	else
	{
		licz=0;
		isFirstValues=0;
	}
	tab[licz]=v;
	if(isFirstValues)
	{
		return -1;
	}
	else
	{
		for(int i=0;i<NUMBER_OF_SAMPLES;i++)
		{
			avarageValue+=tab[i];
		}
		avarageValue=(unsigned int)((float)avarageValue/NUMBER_OF_SAMPLES);
		return avarageValue;
	}
}
/*void ADC_SetActiveChannel(ADC_HandleTypeDef *hadc, uint32_t AdcChannel)
{
  ADC_ChannelConfTypeDef sConfig = {0};
  sConfig.Channel = AdcChannel;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_640CYCLES_5;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  if (HAL_ADC_ConfigChannel(hadc, &sConfig) != HAL_OK)
  {
   Error_Handler();
  }
}*/
