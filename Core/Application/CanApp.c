/*
 * CanApp.c
 *
 *  Created on: Nov 29, 2020
 *      Author: Dawid
 */

#include "CanApp.h"

//_______________________________________________________________
void CanInit(CAN_HandleTypeDef *hcan);
bool CanSendFrame(uint32_t ID, uint8_t* data, uint8_t size);
bool CanRqstData(uint32_t ID);
//________________________________________________________________



CAN_FilterTypeDef sFilterConfig;


void CanInit(CAN_HandleTypeDef *hcan)
{
	/* Function init CAN interface */

	/* Set filter parameters */
	  sFilterConfig.FilterBank = 0;
	  sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
	  sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
	  sFilterConfig.FilterIdHigh = ((0x12B5000C<<3) >>16) & 0xffff;			// THIS MCU RECEIVED ONLY FRAMES WITH EXT ID = 0x12B5000C(IMU FRAMES)
	  sFilterConfig.FilterIdLow = (uint16_t)(0x12B5000C<<3) | CAN_ID_EXT;
	  sFilterConfig.FilterMaskIdHigh = 0xFFFF;
	  sFilterConfig.FilterMaskIdLow = 0xFFF8;
	  sFilterConfig.FilterFIFOAssignment = CAN_RX_FIFO0;
	  sFilterConfig.FilterActivation = ENABLE;
	  sFilterConfig.SlaveStartFilterBank = 14;

	 /* Enable CAN communication */
	  HAL_CAN_ConfigFilter(&hcan1, &sFilterConfig);
	  HAL_CAN_Start(&hcan1);
	  HAL_CAN_ActivateNotification(&hcan1, CAN_IT_RX_FIFO0_MSG_PENDING);

}

bool CanSendFrame(uint32_t ID, uint8_t* data, uint8_t size)
{
	if(size>8)
		return false;

	CAN_TxHeaderTypeDef CanMsg;
	uint32_t TxMailbox=0;

	CanMsg.IDE = CAN_ID_EXT;
	CanMsg.RTR = CAN_RTR_DATA;
	CanMsg.ExtId = ID;
	CanMsg.DLC = size;

	if(HAL_CAN_AddTxMessage(&hcan1, &CanMsg, data, &TxMailbox) == HAL_OK)
		return true;
	else
		return false;

}

bool CanRqstData(uint32_t ID)
{

	CAN_TxHeaderTypeDef CanMsg;
	uint32_t TxMailbox=0;

	CanMsg.IDE = CAN_ID_EXT;
	CanMsg.RTR = CAN_RTR_REMOTE;
	CanMsg.ExtId = ID;
	CanMsg.DLC = 0;			// request frame always have no data

	if(HAL_CAN_AddTxMessage(&hcan1, &CanMsg, 0, &TxMailbox) == HAL_OK)
		return true;
	else
		return false;
}
