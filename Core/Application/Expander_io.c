/*
 * Expander_io.c
 *
 *  Created on: 11 maj 2021
 *      Author: Wiktor,Rudá Moura <ruda.moura@gmail.com>
 *      License: The MIT License (MIT)
 */


#include "Expander_io.h"
#include "Control_output.h"

// Init function (structure of #0 expander, addres E#0,structure of E#1 , addres E#1, i2c )
HAL_StatusTypeDef Input_Output_Init(MCP23017_HandleTypeDef *hdev0,MCP23017_HandleTypeDef *hdev1, I2C_HandleTypeDef *hi2c)
{

	mcp23017_init(hdev0, hi2c, MCP23017_ADDRESS_20); //address 20 = 000 in A2 A1 A0 in MCP23017 pin
	mcp23017_init(hdev1, hi2c, MCP23017_ADDRESS_21); //address 21 = 001 in A2 A1 A0 in MCP23017 pin

	// Ustawienie kierunku pinów - wejsc/wyjsc
	mcp23017_iodir(hdev0, MCP23017_PORTA, MCP23017_IODIR_ALL_INPUT);
	mcp23017_iodir(hdev0, MCP23017_PORTB, MCP23017_IODIR_ALL_OUTPUT);
	mcp23017_iodir(hdev1, MCP23017_PORTA, MCP23017_IODIR_ALL_INPUT);
	// mcp23017_iodir(hdev1, MCP23017_PORTB, MCP23017_IODIR_ALL_OUTPUT); -wyjscia nieporzebne

	// Podciagniecie wejsc do Vdd przez rezystory wbudowane w ekspander
	mcp23017_ggpu(hdev0, MCP23017_PORTA, MCP23017_GPPU_ALL_ENABLED);
	mcp23017_ggpu(hdev1, MCP23017_PORTA, MCP23017_GPPU_ALL_ENABLED);

	if (hdev0 == NULL || hdev1 == NULL)   return HAL_ERROR;
	else return HAL_OK;
}

HAL_StatusTypeDef Input_Output_Function(Input_and_Output *New_Status, Input_and_Output *Old_Status,MCP23017_HandleTypeDef *hdev0,MCP23017_HandleTypeDef *hdev1)
{
	mcp23017_read_gpio(hdev0, MCP23017_PORTA); 				// odczyt wejsc
	mcp23017_read_gpio(hdev1, MCP23017_PORTA);

	New_Status->E0GpioA = ~hdev0->gpio[MCP23017_PORTA]; 	// przypisanie wejsc do struktury
	New_Status->E1GpioA = ~hdev1->gpio[MCP23017_PORTA];

	Mr_Pawel_Function(New_Status, Old_Status);

	hdev0->gpio[MCP23017_PORTB] = New_Status->E0GpioB; 		// zapis do struktury ekspandera
	//hdev1->gpio[MCP23017_PORTB] = New_Status->E1GpioB; 	// wylaczony bo wyjscia nieuzywane


	mcp23017_write_gpio(hdev0, MCP23017_PORTB); 			// wgranie do ekspandera wyjsc
	//	mcp23017_write_gpio(hdev1, MCP23017_PORTB);


return HAL_OK;
}

void mcp23017_init(MCP23017_HandleTypeDef *hdev, I2C_HandleTypeDef *hi2c, uint16_t addr)
{
	hdev->hi2c = hi2c;
	hdev->addr = addr << 1;
}

HAL_StatusTypeDef mcp23017_read(MCP23017_HandleTypeDef *hdev, uint16_t reg, uint8_t *data)
{
	return HAL_I2C_Mem_Read(hdev->hi2c, hdev->addr, reg, 1, data, 1, I2C_TIMEOUT);
}

HAL_StatusTypeDef mcp23017_write(MCP23017_HandleTypeDef *hdev, uint16_t reg, uint8_t *data)
{
	return HAL_I2C_Mem_Write(hdev->hi2c, hdev->addr, reg, 1, data, 1, I2C_TIMEOUT);
}

HAL_StatusTypeDef mcp23017_iodir(MCP23017_HandleTypeDef *hdev, uint8_t port, uint8_t iodir)
{
	uint8_t data[1] = {iodir};
	return mcp23017_write(hdev, REGISTER_IODIRA|port, data);
}

HAL_StatusTypeDef mcp23017_ipol(MCP23017_HandleTypeDef *hdev, uint8_t port, uint8_t ipol)
{
	uint8_t data[1] = {ipol};
	return mcp23017_write(hdev, REGISTER_IPOLA|port, data);
}

HAL_StatusTypeDef mcp23017_ggpu(MCP23017_HandleTypeDef *hdev, uint8_t port, uint8_t pu)
{
	uint8_t data[1] = {pu};
	return mcp23017_write(hdev, REGISTER_GPPUA|port, data);
}

HAL_StatusTypeDef mcp23017_read_gpio(MCP23017_HandleTypeDef *hdev, uint8_t port)
{
	uint8_t data[1];
	HAL_StatusTypeDef status;
	status = mcp23017_read(hdev, REGISTER_GPIOA|port, data);
	if (status == HAL_OK)
		hdev->gpio[port] = data[0];
	return status;
}

HAL_StatusTypeDef mcp23017_write_gpio(MCP23017_HandleTypeDef *hdev, uint8_t port)
{
	uint8_t data[1] = {hdev->gpio[port]};
	return mcp23017_write(hdev, REGISTER_GPIOA|port, data);
}

