/*
 * AsynchActionsManager.h
 *
 *  Created on: Nov 24, 2020
 *      Author: Dawid
 */

#ifndef APPLICATION_ASYNCHACTIONSMANAGER_H_
#define APPLICATION_ASYNCHACTIONSMANAGER_H_

#include <stdbool.h>
#include "AsynchActionsManager.h"
#include "adc.h"
#include "terminal.h"
#include "thermistor.h"
#include "Expander_io.h" //For Input function
#include "Control_output.h" //For input function
#include "ds18b20.h"
#include "usart.h"
#include "twillight.h"

void AsynchActionsHandler(void);

typedef struct{				// struct to store flags for triggering asynchronous actions

	volatile bool ThermistorFlag;
	volatile bool InputFlag;
	volatile bool ThrottleFlag;
	volatile bool TemperatureSensorFlag;
	volatile bool TwillightFlag;

}ActionsFlags;


#endif /* APPLICATION_ASYNCHACTIONSMANAGER_H_ */
