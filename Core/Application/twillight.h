/*
 * twillight.h
 *
 *  Created on: Apr 19, 2021
 *      Author: Olson
 */

#ifndef APPLICATION_TWILLIGHT_H_
#define APPLICATION_TWILLIGHT_H_

#include <main.h>
#include <stdbool.h>
#include "throttleMeasurement.h"

#define DIFFERENCE_HYSTERESIS 3
#define VALUE_MAX_ADC_12BIT 4095.0
#define VALUE_MAX_ADC_8BIT 255.0
#define DEFAULT_VALUE_CHANGE 100
#define NUMBER_OF_SAMPLES 10
typedef enum{
	ADC_OK=0,
	ADC_ERROR
}ADC_Status;
typedef struct{
	uint16_t adcValue; 		//12bit value
	uint16_t avarageValue;
	uint8_t twillightLevel; //8bit value from AccValue
	bool isLightOn; 		//light control
	bool previousIsLightOn;
	uint8_t limitThresholdOn;
	uint8_t limitThresholdOff;
}TLight_Data_t;
//uint8_t avarageValue[NUMBER_OF_SAMPLES];

extern TLight_Data_t ADCTest;

void twillightInit(ADC_HandleTypeDef *hadc,TLight_Data_t *t,bool isDefaultValueOn,uint8_t limitThresholdOn);
ADC_Status twillightLoop(TLight_Data_t *t,ADC_HandleTypeDef *hadc);
//void ADC_SetActiveChannel(ADC_HandleTypeDef *hadc, uint32_t AdcChannel);
#endif /* INC_TWILLIGHT_H_ */
