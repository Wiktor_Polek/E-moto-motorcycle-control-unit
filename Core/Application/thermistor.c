
#include "thermistor.h"
#include "adc.h"

void NTC_loop();
static void NTC_temp_handler(adcData);
static float tableUpdate(uint16_t[], uint16_t);
static void ADC_SetActiveChannel(ADC_HandleTypeDef*);

uint8_t i = 0;
uint16_t divideBy = 0;
uint16_t tableSum = 0;
uint16_t meanAdc = 0;
//float RT;
adcData ThermistorData = {0};

static float tableUpdate(uint16_t table[], uint16_t Adc)
{
	if(i >= numOfSamples)
	{
		i=0;
	}

	table[i] = Adc;
	i++;

	tableSum = 0;
	divideBy = 0;

	for(uint8_t j = 0; j<=numOfSamples-1;j++)
	{
		if(table[j] != 0)
		{
			tableSum = tableSum + table[j];
			divideBy = divideBy +1;				// instead just set divideBy = 10;
		}
	}
	meanAdc = tableSum / divideBy;

	return B/(log(R1/R0) + log((4096.0 - meanAdc)/meanAdc) + B/T0) - 273.0;
}


void NTC_loop()
{

	ADC_SetActiveChannel(&hadc1);
	HAL_ADC_Start(&hadc1);


	if(HAL_ADC_PollForConversion(&hadc1, 3) == HAL_OK)
	{
		ThermistorData.ADC = HAL_ADC_GetValue(&hadc1);
		//RT = R0 * exp(B*(1/adcDataOne.T - 1/T0));  // THERMISTOR RESISTANCE - RT
	}
	ThermistorData.tempAfterFiltration = tableUpdate(ThermistorData.adcTable, ThermistorData.ADC);
	NTC_temp_handler(ThermistorData);
}

static void ADC_SetActiveChannel(ADC_HandleTypeDef* hadc)
{
  ADC_ChannelConfTypeDef sConfig = {0};
  sConfig.Channel = ADC_CHANNEL_5;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_640CYCLES_5;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  if (HAL_ADC_ConfigChannel(hadc, &sConfig) != HAL_OK)
  {
   Error_Handler();
  }
}


static void NTC_temp_handler(adcData ONE)
{

    if(ONE.tempAfterFiltration >= MaxAcceptedTemp)
    {
    	ONE.temp_alarm = true; // temperature is to HIGH
    }else
    {
    	ONE.temp_alarm = false; // temperature is OK
    }

}

