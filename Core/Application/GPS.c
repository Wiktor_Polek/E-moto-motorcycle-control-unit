#include "gps.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "inttypes.h"


struct gps_state gps_init(UART_HandleTypeDef * uart){
	struct gps_state state;

	state.uart = uart;
	for(uint8_t i=0; i<100; i++) state.line_buffer[i] = '\0';
	state.writer_position = 0;
	state.reader_position = 0;
	for(uint8_t i=0; i<30; i++) state.field_buffer[i] = '\0';
	state.field_position = 0;

	state.date_day = 0;
	state.date_mounth = 0;
	state.date_year = 0;
	state.time_hour = 0;
	state.time_min = 0;
	state.time_sec = 0;

	state.latitude = 0.0;
	state.longitude = 0.0;
	state.altitude = 0.0;

	state.status = 0;

	return state;
}

void gps_recv_char(struct gps_state * state, uint8_t recv_char){
	if(state->writer_position == 0 && recv_char == '$') {
		state->writer_position++;
	} else if (state->writer_position >= 1 && state->writer_position < 99) {
		if(recv_char == '\r' || recv_char == '\n') {
			state->line_buffer[state->writer_position - 1] = '\0';
			state -> writer_position = 0;
			gps_process_line(state);

		}else {
			state->line_buffer[state->writer_position - 1] = recv_char;
			state->writer_position++;

	}
	} else {
		state->writer_position = 0;
	}
}


void gps_read_field(struct gps_state * state)
{
	state->field_position=0;
	while(state->line_buffer[state->reader_position] != ','
			&& state->line_buffer[state->reader_position] != '\0'
			&& state->field_position < 29)
	{
		state->field_buffer[state->field_position] = state->line_buffer[state->reader_position];
		state->reader_position++;
		state->field_position++;
	}

	state->field_buffer[state->field_position] = '\0';
	state->reader_position++;
}

void gps_process_gpgga(struct gps_state *state)
{
	gps_read_field(state);
	gps_read_field(state);
	gps_read_field(state);
	gps_read_field(state);
	gps_read_field(state);
	gps_read_field(state);
	gps_read_field(state);
	gps_read_field(state);
	gps_read_field(state);
	if(strlen((const char*)state->field_buffer)>0)sscanf((const char*)state->field_buffer, "%lf", &(state->altitude));


}

void gps_process_line(struct gps_state * state)
{
	state->reader_position = 0;
	gps_read_field(state);
	if(strcmp((const char*)state->field_buffer,"GPRMC") == 0) gps_process_gprmc(state);
	else if(strcmp((const char*)state->field_buffer,"GPGGA") == 0) gps_process_gpgga(state);
	else if(strcmp((const char*)state->field_buffer,"GPGLL") == 0) gps_process_gpgll(state);


}


void gps_process_gprmc(struct gps_state * state)
{
	gps_read_field(state);
	if(strlen((const char*)state->field_buffer) > 0)
	{
		uint32_t tmp;
		sscanf((const char*)state->field_buffer, "%lu", &tmp);
		state->time_sec = tmp % 100;
		state->time_min = (tmp/100) % 100;
		state->time_hour = (tmp/10000) % 100;
	}

	gps_read_field(state);

	gps_read_field(state);
	if(strlen((const char*)state->field_buffer)>0) sscanf((const char*)state->field_buffer, "%lf", &(state->latitude));

	gps_read_field(state);


	gps_read_field(state);
	if(strlen((const char*)state->field_buffer)>0) sscanf((const char*)state->field_buffer, "%lf", &(state->longitude));
	gps_read_field(state);


	gps_read_field(state);
	gps_read_field(state);

	gps_read_field(state);
	if(strlen((const char*)state->field_buffer) > 0)
	{
		uint32_t tmp;
		sscanf((const char*)state->field_buffer, "%ld", &tmp);
		state->date_year = tmp % 100;
		state->date_mounth = (tmp/100) % 100;
		state->date_day = (tmp/10000) % 100;
	}


}

void gps_process_gpgll(struct gps_state * state)
{
	gps_read_field(state);
	gps_read_field(state);
	gps_read_field(state);
	gps_read_field(state);
	gps_read_field(state);

	gps_read_field(state);
	if(((const char*)state->field_buffer)=='A') state->status=1; else state->status = 0;
}




