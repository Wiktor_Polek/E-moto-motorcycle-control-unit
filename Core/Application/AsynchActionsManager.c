/*
 * AsynchActionsManager.c
 *
 *  Created on: Nov 24, 2020
 *      Author: Dawid, Wiktor
 */

#include "AsynchActionsManager.h"

//Import variable for Input Function
extern Input_and_Output IO_actual, IO_old;
extern MCP23017_HandleTypeDef Expander0, Expander1;
extern TLight_Data_t ADCTest;

ActionsFlags Flags;

void AsynchActionsHandler(void)
{

	if (Flags.ThermistorFlag) {
		// TAKE SOME ACTION
		Flags.ThermistorFlag = false;  // always reset flag at the end of action
		NTC_loop();
		HAL_UART_Transmit_IT(&huart2, "Motocontrol!!!\n\r", 16);

		//uart_send(_INFO_, "Motocontrol Loop!!!", 1.0, 1.0);
	}

	if (Flags.InputFlag) {
		Input_Output_Function(&IO_actual, &IO_old, &Expander0, &Expander1); //GPIO function
		Flags.InputFlag = false;
	}

	if (Flags.ThrottleFlag) {
		throttle_read();
		Flags.ThrottleFlag = false;
	}

	if (Flags.TwillightFlag){
		twillightLoop(&ADCTest, &hadc1);
		Flags.TwillightFlag = false;
	}

	if (Flags.TemperatureSensorFlag) {
		DS18B20_ReadAll();
		DS18B20_StartAll();
		DS18B20_ReadTemperature();
		Flags.TemperatureSensorFlag = false;
	}
}
