/*
 * terminal.h
 *
 *  Created on: 5 maj 2021
 *      Author: Adam Witczak
 */

#ifndef APPLICATION_TERMINAL_H_
#define APPLICATION_TERMINAL_H_

#include "stm32l4xx_hal.h"

UART_HandleTypeDef huart2;
typedef enum {_OK_,_ERR_,_INFO_} MSG_TYPE;

void uart_send(MSG_TYPE msg, char info[], double value1, double value2);

#endif /* TERMINAL_H_ */
