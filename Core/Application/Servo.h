/*
 * Servo.h
 *
 *  Created on: Jul 13, 2021
 *      Author: Wiktor, Piotr Duba
 */

#ifndef APPLICATION_SERVO_H_
#define APPLICATION_SERVO_H_

#include <stdint.h>

// 	  set_lamp_angle(lamp_angle); <- to wywolaj w mainie aby dzialalo

// Variable
volatile uint16_t lamp_angle;

#define TIM_NO htim1
#define TIM_CH_NO TIM_CHANNEL_1

// zakresy katowe
#define ANGLE_MIN 0
#define ANGLE_MAX 1800

// zakres PWM - nalezy dobrac do poszczegolnego serva
#define PWM_MIN 500
#define PWM_MAX 2300

#define STEP ((1000 * (PWM_MAX - PWM_MIN)) / (ANGLE_MAX - ANGLE_MIN))

void set_lamp_angle(uint16_t ang);


#endif /* APPLICATION_SERVO_H_ */
