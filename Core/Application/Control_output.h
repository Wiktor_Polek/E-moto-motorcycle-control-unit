/*
 * Control_output.h
 *
 *  Created on: 19 maj 2021
 *      Author: Wiktor
 */

#ifndef APPLICATION_CONTROL_OUTPUT_H_
#define APPLICATION_CONTROL_OUTPUT_H_

#include "Expander_io.h"
#include "stdbool.h"

typedef struct
		{
	uint32_t time;
	bool Warning;
	bool Left;
	bool Right;
	bool first_time_warning;
	uint8_t DriveMode;
	bool Navigation_L;
	bool Navigation_R;
} Control_structure;

HAL_StatusTypeDef Mr_Pawel_Function(Input_and_Output *eio,Input_and_Output *eio_old); //main functin of input and output control
HAL_StatusTypeDef Indicator_Function(Input_and_Output *eio);

/*
 * 	Expander 0
 * 		GPIOA - IN				GPIOB - OUT
 *	0	Side lights				Side lights
 *	1	Dipped beams			Dipped beams
 *	2	Full beams				Full beams
 *	3	Blinkee					Stop light
 *	4	Warning lights			Starter
 *	5	Left indicator			Left indicator
 *	6	Right indicator			Right indicator
 *	7	Horn					Horn
 *
 * 	Expander 1
 * 		GPIOA - IN				GPIOB - OUT
 *	0	Front brake				-----
 *	1	Rear brake				-----
 *	2	Driving mode			-----
 *	3	Navigation button L		-----
 *	4	Navigation button R		-----
 *	5	Starter					-----
 *	6	-----					-----
 *	7	-----					-----
*/



#endif /* APPLICATION_CONTROL_OUTPUT_H_ */
