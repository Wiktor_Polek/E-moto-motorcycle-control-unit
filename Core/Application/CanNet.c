/*
 * CanNet.c
 *
 *  Created on: 19 gru 2020
 *      Author: Dawid
 */

#include "CanNet.h"

/* DATA TO SEND VIA CAN-bus */
extern volatile struct gps_state gps_handle;
extern volatile Control_structure Controls;
extern adcData ThermistorData;
extern Input_and_Output IO_actual;
extern throttleData td;
extern Ds18b20Sensor_t ds18b20[_DS18B20_MAX_SENSORS];
extern TLight_Data_t ADCTest;

/* DATA RECEIVED VIA CAN-bus */
extern volatile uint16_t lamp_angle;
/* ________________________ */

volatile DashBrdFramesID_en ID_table[DSB_MAX_ENUM] = {EXTERNAL_SENSORS_DATA, MOTO_LAT_PARAMETERS, MOTO_LON_PARAMETERS, GPS_DATA};
volatile uint8_t Tab_ind = 0;
volatile gps_val coordinate = {0};

static void coord_to_bytes(volatile gps_val *coords)
{
	long long int moto_lat_temp = abs( ((int)(coords->dblVal * 10000000)));
    coords->bytes[4] = (uint8_t)(moto_lat_temp >> 32);
    coords->bytes[3] = (uint8_t)(moto_lat_temp>>24);
    coords->bytes[2] = (uint8_t)(moto_lat_temp>>16);
    coords->bytes[1] = (uint8_t)(moto_lat_temp>>8);
    coords->bytes[0] = (uint8_t)(moto_lat_temp&0xFF);

    if((coords->dblVal) >= 0) coords->sign = 0;
    else coords->sign = 1;

    if(abs((int)(coords->dblVal)) > 0 && abs((int)(coords->dblVal)) < 10)	 		coords->decSign = 8;
    else if(abs((int)(coords->dblVal)) >= 10 && abs((int)(coords->dblVal)) < 100) 	coords->decSign = 7;
    else if(abs((int)(coords->dblVal)) >= 100 && abs((int)(coords->dblVal)) <= 180)	coords->decSign = 6;
}


void CanSendDashboardData(void)
{
	uint8_t frame[8];

	switch(ID_table[Tab_ind])
	{
		case EXTERNAL_SENSORS_DATA:
		{
			uint8_t driving_mode = Controls.DriveMode;
			float engine_temp = ThermistorData.tempAfterFiltration;
			uint8_t brightness = ADCTest.twillightLevel;
			float air_temperature = ds18b20[0].Temperature;
			uint8_t throttle_signal = td.resultingThrottle;
			uint8_t input_signals = 0;
			uint8_t lights_parameters = 0;

			/* CHECK INPUTS DATA */

			input_signals |= ((IO_actual.E0GpioA & HORN_IN) ? HORN_BM : 0x00);							// HORN
			input_signals &=~ ((IO_actual.E0GpioA & HORN_IN) ? 0x00 : HORN_BM);

			input_signals |= ((IO_actual.E1GpioA & DRIVING_MODE_IN) ? BUT_1_BM : 0x00);					// DRIVING MODE BUTTON (???)
			input_signals &=~ ((IO_actual.E1GpioA & DRIVING_MODE_IN) ? 0x00 : BUT_1_BM);

			input_signals |= ((IO_actual.E1GpioA & NAVIGATION_BUTTON_L_IN) ? BUT_2_BM : 0x00);			// LEFT NAVIGATION BUTTON
			input_signals &=~ ((IO_actual.E1GpioA & NAVIGATION_BUTTON_L_IN) ? 0x00 : BUT_2_BM);

			input_signals |= ((IO_actual.E1GpioA & NAVIGATION_BUTTON_R_IN) ? BUT_3_BM : 0x00);			// RIGHT NAVIGATION BUTTON
			input_signals &=~ ((IO_actual.E1GpioA & NAVIGATION_BUTTON_R_IN) ? 0x00 : BUT_3_BM);

			input_signals |= ((ThermistorData.temp_alarm) ? WARNING_STATUS : 0x00);						// WARNING STATUS (currently indicates temperature alarm)
			input_signals &=~ ((ThermistorData.temp_alarm) ? 0x00 : WARNING_STATUS);

			input_signals |= ((gps_handle.status) ? GPS_CONN_STATUS : 0x00);							// GPS STATUS (indicates whether GPS data are valid)
			input_signals &=~ ((gps_handle.status) ? 0x00 : GPS_CONN_STATUS);

			input_signals |= ECU_CONN_BM;  																// connection status between stm32 and dash-board (set up to OK for now)


			/* CHECK LIGHTS DATA */

			lights_parameters |= ((IO_actual.E0GpioA & LEFT_INDICATORS_IN) ? LEFT_IND_BM : 0x00);		// LEFT IND
			lights_parameters &=~ ((IO_actual.E0GpioA & LEFT_INDICATORS_IN) ? 0x00 : LEFT_IND_BM);

			lights_parameters |= ((IO_actual.E0GpioA & RIGHT_INDICATORS_IN) ? RIGHT_IND_BM : 0x00);		// RIGHT IND
			lights_parameters &=~ ((IO_actual.E0GpioA & RIGHT_INDICATORS_IN) ? 0x00 : RIGHT_IND_BM);

			lights_parameters |= ((IO_actual.E0GpioA & WARNING_LIGHTS_IN) ? HAZARD_LIGHTS_ST : 0x00);	// HAZARD LIGHTS
			lights_parameters &=~ ((IO_actual.E0GpioA & WARNING_LIGHTS_IN ) ? 0x00 : HAZARD_LIGHTS_ST);

			lights_parameters |= (((IO_actual.E1GpioA & FRONT_BRAKE_IN) || (IO_actual.E1GpioA & REAR_BRAKE_IN)) ? BRAKE_LIGHTS_BM : 0x00);	// BRAKE LIGHTS
			lights_parameters &=~ (((IO_actual.E1GpioA & FRONT_BRAKE_IN) || (IO_actual.E1GpioA & REAR_BRAKE_IN)) ? 0x00 : BRAKE_LIGHTS_BM);

			lights_parameters |= ((IO_actual.E0GpioA & DIPPED_LIGHTS_IN) ? DRV_LIGHTS_BM : 0x00);	  	 // DRIVING LIGHTS (Krotkie)
			lights_parameters &=~ ((IO_actual.E0GpioA & DIPPED_LIGHTS_IN ) ? 0x00 : DRV_LIGHTS_BM);

			lights_parameters |= ((IO_actual.E0GpioA & SIDE_LIGHTS_IN) ? PARK_LIGHTS_BM : 0x00);	   	// PARK LIGHTS (Postojowki)
			lights_parameters &=~ ((IO_actual.E0GpioA & SIDE_LIGHTS_IN ) ? 0x00 : PARK_LIGHTS_BM);

			lights_parameters |= ((IO_actual.E0GpioA & FULL_BEAMS_IN) ? BEAM_LIGHTS_BM : 0x00);	      	 // BEAM LIGHTS (Dlugie)
			lights_parameters &=~ ((IO_actual.E0GpioA & FULL_BEAMS_IN) ? 0x00 : BEAM_LIGHTS_BM);

			/* CHECK LIGHTS DATA */

			frame[0] = driving_mode;
			frame[1] = engine_temp;
			frame[2] = brightness;
			frame[3] = air_temperature;
			frame[4] = throttle_signal;
			frame[5] = input_signals;
			frame[6] = lights_parameters;
			frame[7] = 0;

			CanSendFrame((uint32_t)EXTERNAL_SENSORS_DATA, frame, 8);
		}
		break;

		case MOTO_LAT_PARAMETERS:
		{
			coordinate.dblVal = gps_handle.latitude;
			coord_to_bytes(&coordinate);

			for(uint8_t i = 0; i < 5; i++)
			{
				frame[i] = coordinate.bytes[i];
			}

			frame[5] = coordinate.decSign;
			frame[6] = coordinate.sign;
			frame[7] = 0;

			CanSendFrame((uint32_t)MOTO_LAT_PARAMETERS, frame, 8);
		}
		break;

		case MOTO_LON_PARAMETERS:
		{
			coordinate.dblVal = gps_handle.longitude;
			coord_to_bytes(&coordinate);

			for(uint8_t i = 0; i < 5; i++)
			{
				frame[i] = coordinate.bytes[i];
			}

			frame[5] = coordinate.decSign;
			frame[6] = coordinate.sign;
			frame[7] = 0;

			CanSendFrame((uint32_t)MOTO_LON_PARAMETERS, frame, 8);
		}
		break;

		case GPS_DATA:
		{
			uint8_t day = gps_handle.date_day;
			uint8_t month = gps_handle.date_mounth;
			uint8_t year = gps_handle.date_year;
			uint8_t hour = gps_handle.time_hour;
			uint8_t minute = gps_handle.time_min;

			frame[0] = day;
			frame[1] = month;
			frame[2] = year;
			frame[3] = hour;
			frame[4] = minute;
			frame[5] = 0;
			frame[6] = 0;
			frame[7] = 0;

			CanSendFrame((uint32_t)GPS_DATA, frame, 8);
		}
		break;

		default:
			break;
	}// end switch

	Tab_ind++;

	if(!(Tab_ind < (uint8_t)DSB_MAX_ENUM))
		Tab_ind = 0;
}
