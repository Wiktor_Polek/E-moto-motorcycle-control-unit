/*
 * TimeManager.h
 *
 *  Created on: 13 lis 2020
 *      Author: Dawid
 */

#ifndef APPLICATION_TIMEMANAGER_H_
#define APPLICATION_TIMEMANAGER_H_

#include <stdint.h>
#include "stm32l4xx_hal.h"

#include "CanApp.h"
#include "AsynchActionsManager.h"


/* definitions of os delays for each action in tick unit */

#define			INPUT_DELAY				2
#define			THERMISTOR_DELAY		5
#define			THROTTLE_DELAY			2
#define			TMPSENSOR_DELAY			50
#define 		TWILLIGHT_DELAY			2

typedef struct
{
	volatile int InputsTick;
	volatile int ThermistorTick;
	volatile int ThrottleTick;
	volatile int TemperatureSensorTick;
	volatile int TwillightTick;

}DelayTicks;

void TimeManagerInit(TIM_HandleTypeDef * ChosenTimer);
void TimeControl(DelayTicks* Ticks);
void IWDG_Refresh(IWDG_HandleTypeDef *Watchdog);

#endif /* INC_TIMEMANAGER_H_ */
