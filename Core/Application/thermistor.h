
#ifndef APPLICATION_THERMISTOR_H_
#define APPLICATION_THERMISTOR_H_


#include <stdint.h>
#include <math.h>
#include <stdbool.h>
#include "stm32l4xx_hal.h"


#define R1  200.0f                  // OHM
#define R0 10000.0f                 // OHM
#define B  3380.0f
#define MaxAcceptedTemp  363.0f     // K
#define T0 298.0f                   // K
#define numOfSamples 10



typedef struct
{
    uint16_t ADC;
    uint16_t adcTable[10];
    float tempAfterFiltration;
    bool temp_alarm;
}adcData;



void NTC_loop();

#endif
