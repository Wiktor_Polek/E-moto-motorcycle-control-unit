/*
 * throttleMeasurement.c
 */

#include "throttleMeasurement.h"

throttleData td;
int iter_throttle = 0;

long conversion(long x, long in_min, long in_max, long out_min, long out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void ADC_SetActiveChannel(ADC_HandleTypeDef *hadc, uint32_t AdcChannel)
{
  ADC_ChannelConfTypeDef sConfig = {0};
  sConfig.Channel = AdcChannel;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_640CYCLES_5;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  if (HAL_ADC_ConfigChannel(hadc, &sConfig) != HAL_OK)
  {
   Error_Handler();
  }
}

conversionStatus throttle_read()
{
	ADC_SetActiveChannel(&hadc1, ADC_CHANNEL_10);
	HAL_ADC_Start(&hadc1);

	if(HAL_ADC_PollForConversion(&hadc1, 3) == HAL_OK)
	{

		if (iter_throttle < THROTTLE_BUFFER_SIZE)
		{
			td.potValueBuffer[iter_throttle] = HAL_ADC_GetValue(&hadc1); // Get potentiometer value
			iter_throttle++;
		}
		else
		{
			iter_throttle = 0;
			td.potValueBuffer[iter_throttle] = HAL_ADC_GetValue(&hadc1); // Get potentiometer value
			iter_throttle++;
		}

		int temp_sum = 0;

		for (int i = 0; i < THROTTLE_BUFFER_SIZE; i++)
		{
			temp_sum += td.potValueBuffer[i];
		}

		// getting the average
		td.potValue_avg = temp_sum / THROTTLE_BUFFER_SIZE;

		// conversion
		td.resultingThrottle = conversion(td.potValue_avg,0,4030,0,50);

		uart_send(CONV_OK, "Conversion successful!", (double) td.resultingThrottle, (double) td.potValue_avg);

		return CONV_OK;
	}
	else
	{
		uart_send(CONV_ERR, "Conversion failed!", 0, 0);
		return CONV_ERR;
	}
	return CONV_ERR;
}
