/*
 * CanNet.h
 *
 *  Created on: 19 gru 2020
 *      Author: Dawid
 */

#ifndef APPLICATION_CANNET_H_
#define APPLICATION_CANNET_H_

#include <stdbool.h>
#include <stdlib.h>

#include "CanApp.h"
#include "GPS.h"
#include "thermistor.h"
#include "Expander_io.h"
#include "Control_output.h"
#include "throttleMeasurement.h"
#include "ds18b20.h"
#include "twillight.h"

/* BIT MASKS FOR INPUT SIGNALS CAN FRAME */
#define 	HORN_BM				(1 << 0)
#define 	BUT_1_BM			(1 << 1)
#define 	BUT_2_BM			(1 << 2)
#define		BUT_3_BM			(1 << 3)
#define		ECU_CONN_BM 		(1 << 4)
#define 	GPS_CONN_STATUS 	(1 << 5)	// TO DO !!!
#define		WARNING_STATUS		(1 << 6)

/* BIT MASKS FOR LIGHTS SIGNALS CAN FRAME */
#define 	BEAM_LIGHTS_BM		(1 << 0)
#define 	DRV_LIGHTS_BM		(1 << 1)
#define 	PARK_LIGHTS_BM		(1 << 2)
#define		BRAKE_LIGHTS_BM		(1 << 3)
#define		LEFT_IND_BM 		(1 << 4)
#define 	RIGHT_IND_BM	 	(1 << 5)
#define		HAZARD_LIGHTS_ST    (1 << 6)


typedef enum
{
	EXTERNAL_SENSORS_DATA = 0x12B50006,
	MOTO_LAT_PARAMETERS = 0x12B50008,
	MOTO_LON_PARAMETERS = 0x12B50009,
	GPS_DATA = 0x12B5000D,
	DSB_MAX_ENUM = 0x04

}DashBrdFramesID_en;

typedef struct
{
	double dblVal;
	uint8_t bytes[5];
	uint8_t sign;
	uint8_t decSign;
}gps_val;

void CanSendDashboardData(void);

#endif /* APPLICATION_CANNET_H_ */
