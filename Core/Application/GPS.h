#ifndef gps_header
#define gps_header


#include "stm32l4xx_hal.h"
#include "stdlib.h"
#include "string.h"
#include "usart.h"
#include <stdbool.h>



struct gps_state {
	UART_HandleTypeDef * uart;
	uint8_t line_buffer[100];
	uint8_t writer_position;
	uint8_t reader_position;
	uint8_t field_buffer[30];
	uint8_t field_position;

	uint8_t date_day;
	uint8_t date_mounth;
	uint8_t date_year;
	uint8_t time_hour;
	uint8_t time_min;
	uint8_t time_sec;

	double latitude;
	double longitude;
	double altitude;

	char latitude_direction;
	char longitude_direction;

	bool status;



};

struct gps_state gps_init(UART_HandleTypeDef * uart);
void gps_recv_char(struct gps_state * state, uint8_t recv_char);
void gps_read_field(struct gps_state * state);
void gps_process_line(struct gps_state * state);
void gps_process_gprmc(struct gps_state * state);
void gps_process_gpvtg(struct gps_state * state);
void gps_process_gpgsa(struct gps_state * state);
void gps_process_gpgll(struct gps_state * state);

#endif

