/*
 * InterruptsHandler.c
 *
 *  Created on: Nov 24, 2020
 *      Author: Dawid
 */

#include "InterruptsHandler.h"
#include "usart.h"
//_____________________________________________________

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim);
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart);
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan);

//_____________________________________________________

int debug=0;

int time1=0;  // delay testing variables
int time2=0;
int delta_t=7;

/* TIME CONTROL STRUCTURE */
DelayTicks ActionsDelay={1,1,1,1}; // instance of struct uses for time control (should by filled only by "1")

/* CAN COMMUNICATION VARIABLES*/
CAN_RxHeaderTypeDef MyRxHeader;
volatile CanMsg CanBusData = {0}; // can data instance
extern volatile uint16_t lamp_angle;

/* UART COMMUNICATION VARIABLES */
volatile uint8_t recv_char;
volatile struct gps_state gps_handle;

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){


	 if(htim->Instance == TIM7)
		 {
		 	 debug++;
		 	 TimeControl(&ActionsDelay);
		 	 CanSendDashboardData(); // cycling sending data to the dashboard
		 }

}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {


	 if(huart->Instance == USART1)
	 {
		gps_recv_char(&gps_handle, recv_char);
		HAL_UART_Receive_IT(&huart1, &recv_char, 1);
	 }
	if(huart->Instance == USART2)
	{
		// do nothing
	}
} //end of UART callback function


void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan)
{
	//time1=HAL_GetTick();

	if(HAL_CAN_GetRxMessage(&hcan1, CAN_RX_FIFO0, &MyRxHeader, CanBusData.RxData)!=HAL_OK)
	{
		Error_Handler();
	}

	uint32_t FrameID = 0;
	FrameID = MyRxHeader.ExtId;

	if(FrameID == 0x12B5000C)
	{
		lamp_angle = CanBusData.RxData[1]*10;  // receive pitch angle from NET_MCU
	}

	//time2=HAL_GetTick();
	//delta_t=time2-time1;

}//end of CAN callback function
