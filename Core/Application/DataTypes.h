/*
 * DataTypes.h
 *
 *  Created on: 25 lis 2020
 *      Author: Dawid
 */

#ifndef APPLICATION_DATATYPES_H_
#define APPLICATION_DATATYPES_H_

#include "stm32l4xx_hal.h"
#include "stdbool.h"

// FILE JUST FOR INFORMATION WHICH PARAMETERS COULD BE OBTAINED IN THEORY

/*
typedef enum	// Input signals
{
	PRK_LGHT,
	DRV_LGHT,
	BEAM_LGHT,
	BRK_BAR,
	BRK_FOOT,
	IND_L,
	IND_R,
	EMR_LGHT,
	HORN,
	DRV_MODE,
	KCKSTND,
	NAV_L,
	NAV_R,
	NAV_M,

	IN_MAX	//MAX INPUT
}Inputs;

typedef struct { // here we will be store data from simple sensors which uses adc
	//volatile uint16_t voltage_adc;
	//volatile float temp_celsius;
	//volatile float temperature;
}AdcData;

typedef struct // Sensors data
{

	volatile float gps_lat;		//Geo data from GPS device
	volatile float gps_lon;
	volatile uint8_t drivingMode;
	volatile uint8_t CurrentPage; // number of current page on display ???
	volatile float ntc_read;
	volatile bool inputs_data[IN_MAX - 1]; // table of inputs states
	volatile AdcData adc;
}SensorsData;*/

//volatile NetData NetworkData = {0};
//volatile SensorsData DataFromSensors = {0};

#endif /* APPLICATION_DATATYPES_H_ */
