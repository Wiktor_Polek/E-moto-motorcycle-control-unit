/*
 * Control_output.c
 *
 *  Created on: 19 maj 2021
 *      Author: Wiktor
 */

#include "stm32l4xx_hal.h"
#include "Control_output.h"


extern UART_HandleTypeDef huart2;

extern volatile Control_structure Controls;

HAL_StatusTypeDef Mr_Pawel_Function(Input_and_Output *eio,Input_and_Output *eio_old)
{
	/*
	 * Wejscia expandera 0.
	 */
	uint8_t Any_Edge = eio->E0GpioA ^ eio_old->E0GpioA;

	if (Any_Edge)
	{
		if((Any_Edge & SIDE_LIGHTS_IN) != 0 ) 	eio->E0GpioB = eio->E0GpioB ^ SIDE_LIGHTS_OUT;
		if((Any_Edge & DIPPED_LIGHTS_IN) != 0) 	eio->E0GpioB = eio->E0GpioB ^ DIPPED_LIGHTS_OUT;
		if((Any_Edge & FULL_BEAMS_IN) != 0) 	eio->E0GpioB = eio->E0GpioB ^ FULL_BEAMS_OUT;
		if((Any_Edge & BLINKEE_IN) != 0) 		eio->E0GpioB = eio->E0GpioB ^ FULL_BEAMS_OUT;
		if((Any_Edge & WARNING_LIGHTS_IN) != 0)
		{
			Controls.Warning = !(Controls.Warning);
			Controls.first_time_warning = 1;
		}
		if((Any_Edge & LEFT_INDICATORS_IN) != 0) Controls.Left = !(Controls.Left);
		if((Any_Edge & RIGHT_INDICATORS_IN) != 0) Controls.Right = !(Controls.Right);
		if((Any_Edge & HORN_IN) != 0) 			eio->E0GpioB = eio->E0GpioB ^ HORN_IN;
	}

	/*
	 * Wejscia expandera 1.
	 */

	Any_Edge = eio->E1GpioA ^ eio_old->E1GpioA;

	if (Any_Edge)
	{
		if(((Any_Edge & FRONT_BRAKE_IN) != 0) || ((Any_Edge & REAR_BRAKE_IN) != 0) ) eio->E0GpioB = eio->E0GpioB ^ STOP_LIGHT_OUT;
		if((Any_Edge & STARTER_IN) != 0) eio->E0GpioB = eio->E0GpioB ^ STARTER_OUT;
	}

	uint8_t Rise_Edge = eio->E1GpioA & (~eio_old->E1GpioA);

	if(Rise_Edge)
	{
		if ((Rise_Edge & DRIVING_MODE_IN)) Controls.DriveMode = (Controls.DriveMode <= 3) ? Controls.DriveMode++ : 0; // Specjalnie dla Dawida, niech sie zastanawia o co chodzi xd
		if ((Rise_Edge & NAVIGATION_BUTTON_L_IN)) 	Controls.Navigation_L = !(Controls.Navigation_L);
		if ((Rise_Edge & NAVIGATION_BUTTON_R_IN)) 	Controls.Navigation_R = !(Controls.Navigation_R);
	}

	 Indicator_Function(eio); //Funkcja od kierunkowskazow

	// Zapis kopii wejsc do wyznaczenia zboczy
	eio_old->E0GpioA = eio->E0GpioA;
	eio_old->E1GpioA = eio->E1GpioA;

	return HAL_OK;
}

HAL_StatusTypeDef Indicator_Function(Input_and_Output *eio)
{
	if(Controls.first_time_warning == 1)
		{
		eio->E0GpioB = eio->E0GpioB | WARNING_LIGHTS_OUT;
		Controls.time = 0;
		Controls.first_time_warning = 0;
		}

	if (Controls.Left == 1 || Controls.Right == 1 || Controls.Warning == 1)
	{
		if (Controls.time == 0) // pierwsze wykonanie
			{
			Controls.time = HAL_GetTick() + 400;
			if(Controls.Warning == 1) eio->E0GpioB = eio->E0GpioB | WARNING_LIGHTS_OUT;
			else if(Controls.Left == 1) eio->E0GpioB = eio->E0GpioB | LEFT_INDICATORS_OUT;
			else if(Controls.Right == 1) eio->E0GpioB = eio->E0GpioB | RIGHT_INDICATORS_OUT;
			}

		else if (Controls.time < HAL_GetTick()) //gdy doliczy - zmien stan, uruchom od nowa
		{
			Controls.time = HAL_GetTick() + 300;
			if(Controls.Warning == 1) eio->E0GpioB = eio->E0GpioB ^ WARNING_LIGHTS_OUT;
			else if(Controls.Left == 1) eio->E0GpioB = eio->E0GpioB ^ LEFT_INDICATORS_OUT;
			else if(Controls.Right == 1) eio->E0GpioB = eio->E0GpioB ^ RIGHT_INDICATORS_OUT;
		}
	}

	//Jezeli cos wylaczone - to wylacz. Sprawdzane awaryjne jako pierwsze, aby ich nie wylaczylo i else if specjalnie.

	if(Controls.Warning == 0 && Controls.Left == 0 && Controls.Right == 0 ) eio->E0GpioB = eio->E0GpioB & ~WARNING_LIGHTS_OUT;
	else if(Controls.Left == 0 && Controls.Warning == 0) eio->E0GpioB = eio->E0GpioB & ~LEFT_INDICATORS_OUT;
	else if(Controls.Right == 0 && Controls.Warning == 0) eio->E0GpioB = eio->E0GpioB & ~RIGHT_INDICATORS_OUT;


	return HAL_OK;
}

