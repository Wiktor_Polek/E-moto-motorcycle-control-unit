/*
 * throttleMeasurement.h
 *
 *  Created on: 29 cze 2021
 *      Author: awitc
 */

#ifndef APPLICATION_THROTTLE_MEASUREMENT_H_
#define APPLICATION_THROTTLE_MEASUREMENT_H_

#include <stdio.h>
#include "adc.h"
#include "terminal.h"

// resistor values (kOhm)
#define R_1 10
#define R_2 3.3

// size of buffer for the filter of throttle measurement
#define THROTTLE_BUFFER_SIZE 10

typedef enum {CONV_OK,CONV_ERR} conversionStatus;

typedef struct
{
	int potValueBuffer[THROTTLE_BUFFER_SIZE];
	int potValue_avg;
	int resultingThrottle;

} throttleData;

void ADC_SetActiveChannel(ADC_HandleTypeDef *hadc, uint32_t AdcChannel);
long conversion(long x, long in_min, long in_max, long out_min, long out_max);
conversionStatus throttle_read();


#endif /* THROTTLEMEASUREMENT_H_ */
